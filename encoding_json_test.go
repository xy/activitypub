package activitypub

import (
	"testing"
	"time"
)

func Test_JSONWrite(t *testing.T) {
	type args struct {
		b *[]byte
		c []byte
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
		})
	}
}

func Test_JSONWriteActivity(t *testing.T) {
	type args struct {
		b *[]byte
		a Activity
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteActivityJSONValue(tt.args.b, tt.args.a); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteActivityJSONValue() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteBoolProp(t *testing.T) {
	type args struct {
		b *[]byte
		n string
		t bool
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteBoolJSONProp(tt.args.b, tt.args.n, tt.args.t); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteBoolJSONProp() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteComma(t *testing.T) {
	type args struct {
		b *[]byte
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
		})
	}
}

func Test_JSONWriteDurationProp(t *testing.T) {
	type args struct {
		b *[]byte
		n string
		d time.Duration
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteDurationJSONProp(tt.args.b, tt.args.n, tt.args.d); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteDurationJSONProp() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteFloatProp(t *testing.T) {
	type args struct {
		b *[]byte
		n string
		f float64
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteFloatJSONProp(tt.args.b, tt.args.n, tt.args.f); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteFloatJSONProp() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteIRIProp(t *testing.T) {
	type args struct {
		b *[]byte
		n string
		i LinkOrIRI
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteIRIJSONProp(tt.args.b, tt.args.n, tt.args.i); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteIRIJSONProp() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteIntProp(t *testing.T) {
	type args struct {
		b *[]byte
		n string
		d int64
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteIntJSONProp(tt.args.b, tt.args.n, tt.args.d); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteIntJSONProp() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteIntransitiveActivity(t *testing.T) {
	type args struct {
		b *[]byte
		i IntransitiveActivity
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteIntransitiveActivityJSONValue(tt.args.b, tt.args.i); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteIntransitiveActivityJSONValue() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteItemCollection(t *testing.T) {
	type args struct {
		b   *[]byte
		col ItemCollection
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteItemCollectionJSONValue(tt.args.b, tt.args.col); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteItemCollectionJSONValue() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteItemCollectionProp(t *testing.T) {
	type args struct {
		b   *[]byte
		n   string
		col ItemCollection
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteItemCollectionJSONProp(tt.args.b, tt.args.n, tt.args.col); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteItemCollectionJSONProp() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteItemProp(t *testing.T) {
	type args struct {
		b *[]byte
		n string
		i Item
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteItemJSONProp(tt.args.b, tt.args.n, tt.args.i); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteItemJSONProp() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteNaturalLanguageProp(t *testing.T) {
	type args struct {
		b  *[]byte
		n  string
		nl NaturalLanguageValues
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteNaturalLanguageJSONProp(tt.args.b, tt.args.n, tt.args.nl); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteNaturalLanguageJSONProp() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteObject(t *testing.T) {
	type args struct {
		b *[]byte
		o Object
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteObjectJSONValue(tt.args.b, tt.args.o); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteObjectJSONValue() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteProp(t *testing.T) {
	type args struct {
		b    *[]byte
		name string
		val  []byte
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteJSONProp(tt.args.b, tt.args.name, tt.args.val); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteJSONProp() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWritePropName(t *testing.T) {
	type args struct {
		b *[]byte
		s string
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWritePropJSONName(tt.args.b, tt.args.s); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWritePropJSONName() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteQuestion(t *testing.T) {
	type args struct {
		b *[]byte
		q Question
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteQuestionJSONValue(tt.args.b, tt.args.q); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteQuestionJSONValue() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteS(t *testing.T) {
	type args struct {
		b *[]byte
		s string
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
		})
	}
}

func Test_JSONWriteString(t *testing.T) {
	type args struct {
		b *[]byte
		s string
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteStringJSONValue(tt.args.b, tt.args.s); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteStringJSONValue() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteStringProp(t *testing.T) {
	type args struct {
		b *[]byte
		n string
		s string
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteStringJSONProp(tt.args.b, tt.args.n, tt.args.s); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteStringJSONProp() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteTimeProp(t *testing.T) {
	type args struct {
		b *[]byte
		n string
		t time.Time
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteTimeJSONProp(tt.args.b, tt.args.n, tt.args.t); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteTimeJSONProp() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func Test_JSONWriteValue(t *testing.T) {
	type args struct {
		b *[]byte
		s []byte
	}
	tests := []struct {
		name         string
		args         args
		wantNotEmpty bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNotEmpty := JSONWriteJSONValue(tt.args.b, tt.args.s); gotNotEmpty != tt.wantNotEmpty {
				t.Errorf("JSONWriteJSONValue() = %v, want %v", gotNotEmpty, tt.wantNotEmpty)
			}
		})
	}
}

func TestMarshalJSON(t *testing.T) {
	t.Skip("TODO")
}
